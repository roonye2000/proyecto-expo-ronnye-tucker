namespace Ronnye__Chain_of_responsability
{
    //En principió tenemos una clase abstracta
    public abstract class Aprobador
    {
        //Atributo 
        protected Aprobador _siguiente;


        public void AgregarSiguiente(Aprobador aprobador)
        {
            _siguiente = aprobador;
        }
        // Una operación abstracta
        public abstract void Procesar(Compra c);
    }


}
