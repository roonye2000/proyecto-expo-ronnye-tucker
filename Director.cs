using System;

namespace Ronnye__Chain_of_responsability
{   
    //Por último tenemos a nuestro director, nuestro director es el último en el eslabon en la cadena
    public class Director : Aprobador
    {
        public override void Procesar(Compra c)
        {
            Console.WriteLine(string.Format("Compra aprobada por el {0}", this.GetType().Name)); 

        }
    }
}
