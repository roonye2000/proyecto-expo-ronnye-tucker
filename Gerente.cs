using System;

namespace Ronnye__Chain_of_responsability
{   
    //Esto también hereda de aprobador y sobre escribe o implementa la presión abstracta procesada 
    public class Gerente : Aprobador
    {

        public override void Procesar(Compra c)
        {
            //si la operación es <= 1000 va hacer aprobada en este caso por el gerente
          if (c.Importe <= 1000)
            {
                Console.WriteLine(string.Format("Compra aprobada por el {0}", this.GetType().Name)); ;
            }
          else
            {
                _siguiente.Procesar(c);
            }
        }

    }
}
