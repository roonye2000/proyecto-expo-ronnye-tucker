//Librerias
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ronnye__Chain_of_responsability;

//Nombre del proyecto

namespace Ronnye__Chain_of_responsability
{
    class Program
    {
        static void Main(string[] args)
        { 
            //primero definimos los tres objetos que van a ser parte de la cadena
            var comprador = new Comprador();
            var gerente = new Gerente();
            var director = new Director();

            gerente.AgregarSiguiente(director);
            comprador.AgregarSiguiente(gerente);
            //vamos a crear una compra y vamos a empezar con un ciclo 
            var c = new Compra();
            double importe = 1;
            while (importe != 0)
            {
                // si no la puede procesar se la va a derivar al siguiente eslabón de la cadena 
                Console.WriteLine("Ingrese el monto de las compras (Presione 0 para salir)");
                importe = double.Parse(Console.ReadLine());
                c.Importe = importe;
                comprador.Procesar(c);
            }
        }
    }
}
